---
title: "Kick off periode 3"
subtitle: "Week 2"
date: 2018-02-12
tags: ["blog","blogging","cmd","post"]
---

MA 12 FEBRUARI | “PLAN VAN AANPAK”

Na een studiereis naar maastricht zijn we 
allemaal fris en fruitig om aan de nieuwe opdracht 
te beginnen. Het was spannend om te horen te krijgen met 
wie we in een team zitten maar gelukkig viel het heel erg mee. 

Ik hoopte van te voren dat ik bij Eva in het team zou komen omdat 
wij met veel dingen op het zelfde gebied zitten. Voor de rest moeten 
we nog kijken hoe het gaat lopen.

We begonnen met een opzet voor het plan van aanpak en keken in de 
richtlijn wat er allemaal in moet komen. We hebben verschillende memo’s 
gemaakt met per onderwerp wat in het plan van aanpak moet komen. 
Ik vind dit altijd een goede methode om iedereen weg achter de laptop
te krijgen en met zijn alle te brainstormen.
Ik had aangeboden om het plan van aanpak in elkaar te zetten en vorm te geven. 
Ik vind dit zelf altijd leuk om te doen en iedereen was het er mee eens. 
ik had alle informatie verzameld en ging er savonds thuis mee aan de slag.

WOE 14 FEBRUARI | “ZIEK”

Helaas was ik vandaag ziek geworden en kon ik niet bij de studiodag aanwezig 
zijn. Ik heb er wel voor gezorgd dat het plan van aanpak klaar is en het team 
die vandaag kon laten valideren. De validiatie ging goed. De planning zou nog 
iets uitgebreider mogen.