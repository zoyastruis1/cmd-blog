---
title: Kill your darlings
subtitle: maandag 18 september 2017
date: 2017-09-18
---

Onbezorgd kwamen wij maandagochtend de studio binnen.  
Iedereen moest vandaag gaan presenteren. Ik had er zin in. 
Ik vond dat ik een mooie presentatie in elkaar gezet had. 
En trots op wat we in een korte tijd hadden neergezet. Ik merk dat ik het 
altijd erg leuk vind om voor een groep mensen te presenteren maar 
door mijn enthousiasme heb ik dat ik alles ga vertellen. Om dit te voorkomen
heb ik gevraagd of iedereen een eigen stukje wou vertellen.

De presentatie ging goed! Iedereen heeft zijn best gedaan en we stonden
er met zijn alle.

Na de presentatie kregen we feedback van Jantine en Mio. Deze feedback was :

De balans tussen online en offline klopte niet helemaal .
Het is niet binnen 3 minuten te begrijpen, dus niet duidelijk genoeg.
Wat maakt een bordspel zo leuk? onderzoek deze aspecten.

Feedback waar we wat aan hadden! we wisten zelf dat het nog niet perfect was. 
Het was fijn om weer wat frisse feedback van docenten te krijgen.Na de lunch
zouden we hier mee aan de slag gaan..

Lunch time!
Iedereen had iets meegenomen om te delen. Er kwamen appel kruimel taarten 
voorbij, en vegetarische wraps! super lekker.. 
Achteraf gezien snapte ik waarom..

Mio en Jantine kwamen met de vraag of we het concept Kill Your Darlings kennen?
een paar mensen knikte voorzicht van "ja". Bij mij ging er nog geen lampje 
branden. Wat dit betekende? je gooit je hele idee achteraf en je begint gewoon 
opnieuw. Waarom? omdat je eerste idee het beste lijkt maar het goed is om dat 
los te laten en nog dieper op het onderzoek in te gaan. 

Tuurlijk was dit een shock omdat je al 1,5 week naar een idee toe leeft. 
Maar eerlijk gezegd wist ik dat dit voor ons groepje goed zou zijn. 
Ik wist dat er meer in zou zitten. Betere ideeën en beter onderzoek!
