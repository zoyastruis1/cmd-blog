---
title: "Start kwartaal 2"
subtitle: "15 november 2017"
date: 2017-11-15
tags: ["blog","blogging","cmd","post"]
---

Vandaag is de eerste studiodag van kwartaal 2!
De groepjes zijn nog niet bekend en dat vind ik best wel spannend.
In het huidige groepje is alles bekend en kent iedereen zijn rol
om nu weer bij nieuwe mensen terecht te komen vond ik spannend.
Maar wel leuk! De les begon met studiecoaching. De groepjes werden
gelijk bekend gemaakt, mijn nieuwe groepje bestond uit:
Jesse,Deveney en steven. Als opdracht voor de studiecoach moesten we elkaar beter
leren kennen. Per persoon kreeg je een stapeltje post-its. Op elke post-it
moest je iets schrijven over je zelf een "wist-je-dat-je". Als iedereen
er een stuk of 4 gemaakt had maakten je er propjes van. Daarna werden de propjes 
voor gelezen en moesten de teamleden bedenken bij wie van de 4 die hoorden.
Een leuke manier om elkaar een beetje te leren kennen.

Er was ook nog een indivudele opdracht (zie foto) 
In kwartaal in hebben we doelen gesteld. Het was nu de bedoeling
om terug te kijken op dit kwartaal en gelijk doelen voor kwartaal 2
te bepalen. 

Na de studiecoaching konden we beginnen met de nieuwe briefing.
Voor de eerste week stonden er 3 deliverbels op de planning.
Debriefing,merkanalyse,een planning. Deze 3 hebben we verdeeld
en afgesproken dat het voor maandag af moesten. Om dit overzichterlijker
te maken maakten we een trello bord aan. Ik ging aan de slag met de
debriefing samen met steven. 

