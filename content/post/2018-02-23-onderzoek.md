---
title: "Onderzoek"
subtitle: "Week 3"
date: 2018-02-23
tags: ["blog","blogging","cmd","post"]
---

MA 19 FEBRUARI | “ONDERZOEK”

Vandaag hebben we een begin gemaakt aan het onderzoeksplan. 
We hebben een indeling gemaakt over wat er in moet komen. 5 hoofdstukken: 
1. Hoofd en deelvragen
2. Aannamens die we hebben over de doelgroep
3. Onderzoekmethodes
4. Onderzoeksresultaten
5. Conclusies

We liepen al vast bij de hoofd en deelvragen het probleem: 
Jongeren die een ongezonde levensstijl hebben is zo groot dat we niet 
wisten hoe we dit konden indelen in deel groepen. 
Gelukkig werden er woensdag 2 workshops aangeboden en hebben Danielle Eva 
en ik besloten hier heen te gaan. Aan de hand van deze workshops konden we 
verder gaan. Wel hebben we de bronnen die op n@tschool staan verdeeld en 
afgesproken dat iedereen deze bronnen zou bekijken. Woensdag is de deadline 
hiervoor.


WOE 21 FEBRUARI | “VERSCHILLENDE WORKSHOPS”

Op woensdag hebben we ons in geschreven voor 2 workshops. 
Allebei de workshops waren onswijs intressant. In de workshop 
onderzoeksmethode ging over verschillende methodes die je kan toepassen 
als je gaat onderzoeken. Ook werden we geholpen bij het bedenken van de 
hoofd en deelvragen. Als deelvragen hebben we bedacht: Wat is gezond? 
Hoe zorg je voor gedragsverandering? Wat is de huidige leefstijl? Hier gaan 
we deskresearch naar doen. Maar we wouden ook graag weten hoe gezond de 
studenten eigenlijk leven daarom hebben we aan de hand van de workshop 
besloten om de studenten een dagboek bij te laten houden.