---
title: "Tijd voor onderzoek"
subtitle: "woensdag 6 september 2017"
date: 2017-09-06
tags: ["blog","cmd","post"]
---

We begonnen de dag met het maken van een plan van aanpak.
Ik merk dat ik de taak van "teamcaptain" in de groep aanneem. 
Ik heb het gevoel dat mijn teamgenoten het ook fijn vinden dat ik dit doe.
Ik schrijf de dilverbels op die gemaakt moeten worden en vraag daarna wie 
dit op wil pakken. Op deze manier dwing ik ook niemand om iets te doen wat 
ze niet leuk vinden en kan iedereen zelf bepalen wat ze willen laten zien en 
waar ze in willen groeien. Ik merk dat de groep dit ook prettig vind.

**Plan van aanpak voor 6 sep:**
-Resultaten van het onderzoek uitwerken
-Thema kiezen en daar onderzoek naar doen
-Stand up uitwerken

We begonnen de dag met het overleggen van de onderzoeksresultaten.  
Wij hadden de studenten van de opleiding Arts & Crafts gevraagd om een  
enquête in te vullen via google formulieren.Dit werd automatisch omgezet 
in een grafiek.

Wij waren enthousiast aan de slag gegaan met de resultaten. Wij wouden dit gaan 
uitypen in een verslag.Toen kwam Mio naar ons toe en gaf ons feedback. 
Mio"*waarom typen jullie een verslag die misschien maar 1 keer gelezen word.
Jullie zijn CMD'ers bedenk iets creatiefs! iets visueels*""
Visueels? ik vond dat vaag klinken.. Tot er opeens een lichtje ging branden! 
Een infographic, met mijn grafische achtergrond ging mijn ontwerphart hier 
sneller van kloppen.Samen met Emma hebben wij deze taak op ons genomen.
Samen schetsen we eerst iconen bij de resultaten uit het onderzoek en daarna 
gingen we deze individueel uitwerken. Ik ze samen gevoegd en er tekst 
in een leuk lettertype bij gezet en gezorgd dat allemaal bij elkaar past 

![alt tekst](../img/infographic-schets.jpg)


![alt tekst](../img/infographic-def.pdf)
(zie foto).