---
title: "Ontwerpcriteria"
subtitle: "Week 6"
date: 2018-03-19
tags: ["blog","blogging","cmd","post"]
---

MA 19 MAART | “VRIJ”

Vandaag was een vrije dag. De docenten hebben een studiedag en 
we hebben besloten niet naar school te gaan maar vannuit huis te werken.

WO 21 MAART | “ONTWERPCRITERIA”

Vandaag was het even stress. Eva en Danielle moesten hun lifestyle diary 
opnieuw maken en Jesse was er niet. Maar er was ook een validatie voor de 
ontwerpcriteria. Ik heb gezegd dat ik deze op mij zal nemen en begon met het 
bedenken van criteria aan de hand van onderzoeksresultaten. Dit is prima in 
mijn eentje gelukt want tijdens de validatie kreeg ik compliment voor hoe het 
eruit zag als kritiekpunt kreeg ik dat het bij sommige te algemeen is. 

Daarnaast had ik een STARRT uitgeprint omdat ik hier graag feedback op wou. 
Met een groepje gingen we bij elkaar zitten en las Jantien al deze 
STARRTS door. Ik heb feedback gekregen waar ik iets mee kan en ik denk dat
ik nu EINDELIJK een goede starrt kan schrijven.