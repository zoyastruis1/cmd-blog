---
title: de tijd begint te dringen
subtitle: woensdag 27 september
date: 2017-09-27
---

Het is woensdag 27 september, 
over een week op woensdag 4 oktober hebben wij de presentatie over 
ons nieuwe concept. Daarnaast is woensdag 4 oktober de deadline van de 
tweede iteratie.

Het was dus dringend tijd dat we een concept gingen neerzetten.
In de vorige post kon je lezen dat we bezig waren met creatieve technieken. 
Hier uit zijn 3 concepten gekomen maar we hadden nog niet gekozen dat gingen 
wij vandaag doen. Het was een idee om de concepten te combineren we moesten 
even overleggen hoe we dit het beste konden doen. Na druk overleggen en 
discussiëren kwam het concept tot leven. En toen... 

liepen we vast. We hadden geen idee wat we nu moesten doen. 
Er was op dat moment een peercoach aanwezig die hebben we erbij geroepen. 
Ons concept vertelt omdat we benieuwd waren of we op de goede weg zaten. 
Uit het feedback kwam dat hij het een leuk idee vond. 
Hij zei dat het een goed idee was om op dit 
punt een spelverloop te maken. Dat we gaan uittypen hoe het spel werkt op deze 
manier komen we er ook snel genoeg achter wat er klopt en juist wat er niet 
klopt. 

Wat klopte er niet? Er zitten een hoop losse elementen in die allemaal 
heel leuk zijn maar nog niet op elkaar aansluiten.

Toen we het spelverloop hadden gemaakt Vonden we het een goed idee om nog 
een keer feedback te vragen. Deze keer deden we dat ook aan een peercoach, 
Heleen luisterde naar ons spelverloop en had als feedback dat ze zich afvroeg 
of de spelelementen wel genoeg op de doelgroep aansluiten. Dit vonden we een 
heel belangrijk punt want we wouden graag dat ons spel echt aansluit op de 
doelgroep Arts & Crafts. We hebben dit opgelost door de verschillende spellen 
die je in elk spelgebied speelt te verbinden met arts en crafts dus iets 
creatiefs bijvoorbeeld een tekenopdracht of iets in elkaar te zetten.

We begonnen met een schets te maken voor ons paperprototype zodat we die 
donderdag na het werkcollege gelijk iets konden maken. Ik had met Elien het 
schetsen op ons genomen alle schermpjes moesten gemaakt en bedacht worden.

**Donderdag 28 oktober**
We hadden met ons groepje afgesproken om op donderdag
het prototype in elkaar te zetten. Het was een beetje teleurstellend 
want uiteindelijk waren alleen Emma en ik er. Derest was niet gekomen.. 
We stonden hier niet teveel bij stil want er was genoeg te doen. 
We hadden een leuk idee om op foambord een iPad te maken en met twee ringetjes 
er in waar de blaadjes aan vast zitten zodat je die kan omdraaien. 
Dit moest allemaal gemaakt worden en het moest er ook goed uitzien omdat 
we dit bij onze presentatie gingen gebruiken. Uiteindelijk waren we super 
blij met het resultaat.

![alt tekst](../img/prototype-nieuw-1.jpg)


![alt tekst](../img/prototype-nieuw-2.jpg)