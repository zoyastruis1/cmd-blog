---
title: Nieuwe prototype
subtitle: maandag 9 oktober 2017
date: 2017-10-09
---

Vandaag stond in het teken van de feedback die
we op de presentatie hadden gehad mee te nemen in onze 
laatste aanpassingen aan het spel. Ook werd de nieuwe Iteratie uitgelegd.
Deze Iteratie is heel kort en het is ook de bedoeling dat overal de puntjes 
op de I gezet worden. 

We gingen met ons groepje bij elkaar zitten en namen de feedback van 
de presentatie door. We waren er al vrij snel over uit dat het allemaal 
simpeler moest. Er zaten in het spel leuke elementen maar het was *te* veel 
waardoor het spel niet in 3 minuten uit te leggen is. Waardoor je het jezelf
te moeilijk maakt.

We pakten ons paperprototype erbij. En gingen per slide kijken wat we moesten 
vernieuwen of aanpassen. Misschien zelfs weg halen. Nadat het spel zo goed als 
klaar was. Leek het ons een goed idee om het peperprototype digitaal te maken. 
Dit kost net zoveel tijd en geeft toch wel een extra gevoel bij het spel. 
Emma en ik hadden de taken verdeeld. We hadden eerst een layout van een pagina 
gemaakt. Met daarin lettertypes, kleuren en pijltjes en vakjes zodat wij wel 
het zelfde aan het ontwerpen zijn. Nadat dit gedaan is hebben wij ze samen 
gevoegd en nog een keer gekeken of het logisch is etc. Dit is soms lastig 
omdat wij al weken met dit spel bezig zijn voor is het zo duidelijk maar daarom 
moet je niet vergeten om het zo goed mogelijk aan je doelgroep uit te blijven 
leggen. Uiteindelijk heb ik de ditgitale versie in POP by marvel gezet zodat
we op de expo ook iets tastbaars konden laten zien.

![alt tekst](../img/digitaal-1.jpg)

![alt tekst](../img/digitaal-2.jpg)