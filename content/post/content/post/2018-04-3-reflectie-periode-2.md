---
title: "reflectie periode 2"
subtitle: "Week 7"
date: 2018-04-03
tags: ["blog","blogging","cmd","post"]
---


Als ik terug kijk op deze periode merk ik dat ik een enorme groei gemaakt heb 
kwartaal 2 naar kwartaal 3. Ik ben mij heel bewust geworden van mijn goede
kwaliteiten zoals onderzoeken en heb mij daar dit kwartaal in verdiept
door workshops te volgen en meer de breedte in te gaan.

Ik heb ook heel veel geleerd op het gebied van STARRTS schrijven.
Dit kwartaal viel het denkbeeldige kwartje. Dat is niet aan komen waaien,
daar heb ik heel wat bloed zweet en tranen in zitten. Maar het resultaat
hiervan is dat ik alle 4 de competenties heb gehaald tijdens de competentie
tafels. Ik besef nu dat het niet meer om de beroepsproducten draait maar
om je eigen proces tijdens de competentie.

Het samenwerken in het groepje ging dit deel van het kwartaal in ups &
downs. Aan de ene kant is Yes! We can, een onwijs gemotiveerd team met
veel talent maar door persoonlijke omtstandigheden waren we vaak niet
compleet. De keren dat we compleet waren, hebben we hard gewerkt en
deed iedereen zijn best.