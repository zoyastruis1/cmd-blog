---
title: Creatieve technieken
subtitle: maandag 7 mei 2018
date: 2018-05-07
---

MA 7 MEI| “CREATIEVE TECHNIEKEN”

Het was tijd om de onderzoeksfase achter te laten en een goed concept 
te gaan bedenken. Dat is makkelijker gezegd dan gedaan. We hadden heel 
veel inzichten uit het onderzoek en we wisten waar we ons op wilden gaan 
richten. Namelijk peerpressure en eenzaamheid. Ook hebben we al de doelgroep 
aangepast naar basisschool kinderen en hun ouders. We hebben gekozen voor 
een braindump, alle ideeen die we al hadden op te schrijven. Na 5 minuten 
hadden we een tafel vol met memo’s. Met deze memo’s hebben we gekozen voor 
een COCD-box in deze box hebben we alle ideeen geselecteerd.  
De beste ideeen die er uit zijn gekomen, hebben we gecombineerd tot
een concept.

WO 9 MEI | “FEEDBACK OP CONCEPT”

We begonnen met concept uit denken. We liepen tegen wat dingen aan. 
Het leek ons een goed moment om feedback te vragen aan Jantien. 
Alle losse ideeen miste nog wat om 1 sterk concept te maken.  
De feedback die Jantien gaf dat het handig zou zijn om keypersonen 
te hebben die de app aansturen.  Maak een community van de app. 