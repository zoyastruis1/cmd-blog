---
title: Reflectie
subtitle: maandag 4 juni 2018
date: 2018-06-04
---



MA 4 JUNI | “REFLECTIE”

Als ik terug kijk op dit jaar merk ik dat ik een enorme groei gemaakt heb 
Toen ik het dit jaar begon, was ik net afgestudeerd als grafisch vormgever. 
Als vormgever keek ik alleen naar of een ontwerp “mooi” was. 
Ik merk dat ik nu nog -geen jaar- later iets ontwerp omdat het past bij de
doelgroep. En zal ik altijd eerst onderzoek doen, voordat ik begin 
met ontwerpen. Ik heb als CMD’er leren samenwerken met andere ontwerpers. 
Iets wat ik heel lastig vond in het begin van het jaar. Ik wilde alles
zelf doen en ik vertrouwde niemand. Toen ik besefte dat dit een belangrijk 
onderdeel is, als je ontwerper wil worden ben ik het langzaam gaan veranderen. 
Ik vind dat ik nu goed in een team kan samenwerken. Ik laat mensen die nog 
wat willen leren een beroepsproduct maken en doe het niet meer allemaal zelf.
Ik heb ook een ontwikkeling meegemaakt wat voor mijzelf heftig was.
Ik werd door het jaar heen geconfronteerd met mijn faalangst en perfectionisme.
In het derde kwartaal had ik zoveel stress (terwijl dit niet nodig was) dat
ik fysieke klachten kreeg. Mijn team leden en mijn projectdocent Jantien 
zeiden tegen mij dat het op deze manier niet door kon gaan.Vanaf dat moment 
probeer ik een stapje terug te doen in het project. Een lastige opgave als
je alles leuk vind! Dit is niet iets wat in een dag over is en daar ben 
ik me ook bewust van. In jaar 2 ga ik mijzelf hier nog beter in
proberen te ontwikkelen zodat ik een betere CMDer kan worden.