---
title: Design Rationale
subtitle: maandag 21 mei 2018
date: 2018-05-21
---

MA 21 MEI | “2E PINKSTERDAG”

2e Pinksterdag dus geen studio dag

WO 25 MEI | “DESIGN RATIONALE”

Nu de eerste low-fid prototype schetsen gemaakt 
zijn leek het me een goed idee om een design rationale te schrijven. 
Dit was heel lastig maar ik ben er samen met Danielle aan begonnen. 
We hebben eerst een indeling gemaakt en daarna heb ik feedback aan Jantien 
gevraagd. Alle elementen zaten in de design rationale maar de volgorde klopte
nog niet. Ik heb daarna de feedback verwerkt en de rationale aangepast. 
Dit heb ik in mijn eentje gedaan.