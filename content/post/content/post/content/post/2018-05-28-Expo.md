---
title: Expo
subtitle: maandag 28 mei 2018
date: 2018-05-28
---

MA 28 MEI | “PUNTJES OP DE I”

Het was tijd op de puntjes op de I te zetten. 
Er moest voor de expo nog een presentatie poster gemaakt worden 
Eva & ik namen dit op ons. Eva stelde voor dat ze alle tekst ging uittypen 
en ik ga de poster visueel maken. Deze samenwerking was heel goed. 
Omdat ik het visuele gedeelte deed was het fijn dat iemand anders 
nadacht over wat er op moest komen kwa tekst.

WO 30 MEI | “VOORBEREIDEN OP DE EXPO”

De woensdag voor de expo was stress. De high-fid prototype was 
nog niet af de poster was ook nog niet af. Maar we moesten ook na
denken over de stand. Hoe komt het er uit te zien? Wat gaan we neer zetten. 
We wouden een bepaalde sfeer uit stralen alsof je op een festival stond daarom 
dacht ik dat het leuk zou zijn om een markt kraampje te maken. Ik heb alvast 
de overdakking geschilderd en ben daarna verder gegaan met de poster.

VR 1 JUNI | “EXPO”

De dag van de expo was aangekomen! Een klein beetje stress. 
We waren om 10:00 op school gekomen zodat we alles konden 
uitprinten en rustig konden opbouwen. De poster was af en hebben 
hem uitgeprint en gecheckt. Er moesten nog wat dingen aangepast worden en 
daarna kon die op A1 uitgeprint worden. Daarna konden we de stand op
gaan bouwen. Het was lastig omdat de overkapping niet op de stokken 
bleef staan. Dus met veel ducktape en samenwerking is het uiteindelijk 
toch gelukt.