---
title: Aan alles komt een eind
subtitle : 23 oktober 2017
date: 2017-10-23
---

Terug van de herfstvakantie!
De vakantie is super snel gegaan en ik heb deze
vakantie ook goed besteed om de eerste stappen aan mijn 
leerdossier te zetten. 

Daarnaast wil ik het geen frisse start noemen want nog een week en
dan zit ook deze iteratie erop. En zijn we aan het einde van het kwartaal
gekomen. Maar eerst nog de expo! hier moesten nieuwe ideeen voor bedacht
worden. We gingen bij elkaar zitten en iedereen riep zijn of haar ideeen.
Ik bracht dit samen in een schets al vrij snel zie je dan of iets werkt of niet
daarnaast moeten we niet vergeten dat de expo al woensdag is. Vandaag 
moeten er keuzes gemaakt worden.

We kiezen ervoor om op een groot foamboard een infographic over het spel
te maken. Dit doen we niet uitgeprint maar met de hand. Ik heb een schets
gemaakt over hoe het eruit moet komen te zien en ik merk dat de meeste
van mijn teamleden weer met individuele dingen bezig zijn. Op dit moment
kom ik voor mijzelf op en gooi ik in de groep **jongens, deze infographic
ga ik niet alleen maken** maar uiteindelijk heb ik dit wel alleen gedaan
niemand voelde zich blijkbaar geroepen om te helpen. En ik ga zeker niet
smeken om hulp :)
Iedereen was super blij met de gemaakt infographic.

![alt tekst](../img/bord-expo-1.jpg)



Daarnaast moest er ook nog een ontwerpproceskaart gemaakt worden van week
4 tot en met 8. In deze ontwerpproceskaart zie je ook hoe snel de tijd gegaan
is. Je moest heel goed terug denken over wanneer je welke keuzes gemaakt heb
en hoelang dat al geleden is.

![alt tekst](../img/ontwerp-kaart-2.jpg)