---
title: Reflectie kwartaal 1
subtitle: 26 oktober 2017
date: 2017-10-26
---

Het einde van het eerste kwartaal!
Het eerste kwartaal als CMD'er heb ik super
veel geleerd! Dingen zoals onderzoek doen,
prototypes maken, leren samen werken.

De eerste iteratie begon heel goed. Iedereen deed zijn best en
de taken werden goed verdeeld. Ik vond het heel goed dat we
gelijk de eerste week intervieuws gingen houden. Dit is in het begin
best eng maar iedereen pakte dit goed op. Ik was het in deze
iteratie niet met iedereen eens over de keuzes die er gemaakt werden
voor bijvoorbeeld het spel maar ik probeerde mij nog een beetje
op de achtergrond te houden en te luisteren naar wat iedereen wou.
Dit is is een groot verschil met mijn vooropleiding omdat ik daar
altijd kon doen wat ik zelf het beste vond.

De tweede iteratie na de kill your darlings merkte ik dat iedereen
stiekem blij was. Dit is grappig om te zien omdat je wel 1,5 week
met een spel bezig bent maar blijkbaar was dit niet goed genoeg.
Anders was niet iedereen zo blij er mee geweest dat dit idee achter
over gegooid werd. Ik merkte dat ik hier al iets meer de "captain"
was. Er werd ook veel aan mij gevraagd over opdrachten en over
wie wat moest doen. Deze rol nam ik natuurlijk aan. We besteden
ook meer aandacht aan het vooronderzoek. In deze iteratie hebben we ook
veel tijd gestoken in concepten bedenken. Omdat iedereen wist dat we
een beter spel konden bedenken dan in de eerste iteratie.

In deze iteratie ben ik ook wel mijzelf tegen gekomen. Namelijk
ik deed veel te veel werk alleen. Ik pakte alles op en zei tegen mij
team genoten **nee joh, dat doe ik wel** Hierdoor zat ik vaak thuis
nog aan deliverbels te werken terwijl we dit beter hadden kunnen verdelen.
De eerste scheuren in het goede samenwerken waren ook te zien. Doordat
we hadden afgesproken na school met zijn 5en het paperprototype te maken
maar uiteindelijk waren alleen Emma en ik er. Dit soort dingen zorgt
voor irritaties binnen de groep. Ik heb dit wel proberen op te lossen
door gelijk te bespreken dat we het niet netjes vonden dat niet iedereen
er was. (door welke reden dan ook)

De laatste iteratie. Deze iteratie stond in het teken van de puntjes
op de I zetten en deze duurde dan ook maar 1,5 week. Niet veel tijd om
veel veranderingen aan te brengen. Dit was dan ook niet nodig. Na de
presentatie wisten we wat we wilden verandereden. Ook in deze iteratie
merkte ik dat het team moeilijk op gang kwam. Veel lieten ze van Emma en mij 
afhangen en kwamen niet met eigen iniatief. Persoonlijk heb ik deze iteratie
wel veel gedaan. Zoals het paperprototype digitaal te maken.

Daarnaast is ons groepje door loop van de weken heel erg veranderd.
Ik merk dat in het begin iedereen heel enthousiast was en alles wou
doen en eigenlijk aan het einde van het kwartaal vind ik het jammer
dat niet alle team leden alles hebben gegeven. En had er misschien
dat laatste stukje nog net iets meer in gezeten.

Veel dingen die ik over mijzelf heb geleerd in dit kwartaal.
Het is fijn dat ik kennis van de verschillende adobe-pakketen heb.
In dit team ben ik de "captain" geweest en met die titel ben ik heel blij.
Ik ben er dus achtergekomen dat ik het heel leuk vind om
andere te helpen en als mijn team genoten naar mij kwamen omdat ze een vraag
hadden gaf dit wel een goed gevoel.