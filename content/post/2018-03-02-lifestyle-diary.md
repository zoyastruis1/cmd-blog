---
title: "Lifestyle diary"
subtitle: "Week 4"
date: 2018-03-02
tags: ["blog","blogging","cmd","post"]
---

MA 26 FEBRUARI | “BIJNA IEDEREEN ZIEK”

Vandaag waren van de 5 teamleden er 3 ziek. Danielle en ik waren wel
naar school gekomen. We moesten een korte stand-up houden over waar we
op dit moment mee bezig zijn. (Onderzoek en de lifestyle diary) 
Als feedback van Jantien kregen we dat we heel goed bezig zijn en het 
idee van de koelkast (inzicht krijgen in leefstijl studenten) vond ze een 
heel goed idee.

Danielle en ik zijn verder gegaan met het onderzoek wat we in de vakantie 
hebben opgezet. Dit moest nog uitgetypt worden want onze eigen gestelde deadline 
was woensdag. 

WOE 28 FEBRUARI | “LIFESTYLE DIARY”

S De dag begon vandaag met studiecoaching samen met Jantien hebben ze een quiz 
voorbereid en dit ging over het leerdossier en alles wat daarbij komt kijken 
zoals competenties etc. Ons team (Danielle, Eva en ik) hadden de quiz gewonnen! 
Daarna konden we de studiecoaches nuttige vragen stellen over het leerdossier

Daarna konden we verder met de lifestyle diary. Dit moest wel snel want hij 
moest vandaag ook af. We dachten al die tijd die we hem samen (met het team) 
moesten maken. Maar opeens bleek het dat het individueel moest. 
Wat uiteindelijk ook logischer is omdat je allemaal persoonlijk een dagboek 
bij gehouden heb. 

T Mijn taak was om de lifestyle diary in elkaar te zetten. Ik heb de fotos 
al gemaakt deze moesten uitgeprint en opgeplakt worden. Ik had de keus om dit 
op school te gaan doen maar thuis werk ik in een prettiger omgeving dus ben ik 
naar huis gegaan.

A Op school heb ik de fotos uitgeprint en eenmaal thuis aangekomen allemaal 
uitgeknipt. Daarna bedacht hoe ik dit A2 vel in ging delen. Het waren 2 dagen 
dus ik deelde het blad doormidden. Daarna vond ik het handig om het in te delen
in ochtend middag en avond. Daarna was het opplakken en de iconen erbij 
toetevoegen.

R Het resultaat was dat ik na een avond knippen en plakken de lifestyle diary 
in elkaar had gezet. 

R De lifestyle diary is iets rommeliger geworden dan ik eigenlijk wou. 
Maar dit kon niet anders door de hoeveelheid fotos/iconen die er opmoesten 
komen. Daarom ben ik ook niet te streng voor mezelf (deze keer) maar het is een 
duidelijk foto-dagboek geworden met een overzicht van mijn dagen. Hier kunnen 
we wat mee als we deze allemaal naast elkaar gaan leggen in ons groepje.

T De volgende keer wil ik proberen om iets eerder aan de opdracht te beginnen 
zodat ik minder stress heb de volgende keer.