---
title: Puntjes op de i
subtitle: woensdag 11 oktober 2017
date: 2017-10-11
---

Vandaag is de laatste dag voor de vakantie. 
Ik doe mijn best om nog even de concentratie erbij te houden. 
Zeker omdat wij na de herfstvakantie nog maar 1 studiodag hebben 
voordat wij een expositie over ons spel moeten laten zien. 

Ik merk dat het groepje een beetje uit elkaar aan het vallen is.
Een paar team leden beginnen hun concentratie te verliezen en hierdoor
brengen ze minder in het spel. Ik heb het gevoel dat ik en groot deel
doe en veel iniatief neem. Ik mis dit van sommige teamleden.

Waar waren we gebleven?
Het prototype was een app op de iPad geworden. Waar je ook echt kon door
en terug klikken. Aan de hand van dit prototype heb ik de spelregels 
geschreven. Eerst een speluitleg en daarna regels of opmerkingen die je 
MOET weten om het spel te kunnen spelen.

Emma heeft zich bezig gehouden met het spelanalyse uit te typen.
We hebben hier vooral feedback van Mio opgevraagd omdat we merken dat we
van overige klasgenoten niet heel veel feedback krijgen. Veel mensen vinden
dit toch nog lastig om te geven.

Ik vond het jammer dat ondanks de vele aanpassingen en verbeter punten
die we aan de app veranderd hebben, heb ik het idee dat Mio het nog
steeds een ingewikkeld idee vind. Dit kunnen we in zo korte tijd
niet meer aanpassen helaas.Het positieve punt is dat het zo goed
aansluit op de doelgroep. De creatieve opdrachten en het bouwen 
bij de eindopdracht. 
