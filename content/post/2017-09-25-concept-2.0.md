---
title: Conept 2.0
subtitle: maandag 25 september 2017
date: 2017-09-25
---

Vandaag is de dag van concepten bedenken!
Ik was best een beetje trots op ons. 
Iedereen had zijn steentje bijgedragen om ervoor te 
zorgen dat vandaag het interview uitgewerkt zou zijn. 
Ik zelf ben niet zo goed in verslagen typen dus ik gaf aan dat ik 
het onderzoek visueel wou maken. 

Toen we maandag begonnen konden we verder gaan met een ontwerpproceskaart. 
Een kaart waarin je zelf met woorden en lijntjes kon aangeven hoe je proces 
verlopen is. Dit is voor ons groepje ook fijn zodat we ook kunnen krijgen op 
wat we anders kunnen doen, op welk moment.

![alt tekst](../img/ontwerp-kaart.jpg)

Verder was het tijd om met creatieve technieken een nieuw concept te bedenken. 
Wij gebruikten de COCD-box. Op een A3 vel tekende we een groot vierkant. 
Met links boven realiseerbaar, links beneden niet realiseerbaar. 
En dan in het linkse vak origineel ideeën en in het rechtse vak 
niet-originele ideeën. Iedereen kon ideeën roepen. Niks was slecht en 
alles werd opgeschreven. Toen iedereen zijn ideeën had opgeschreven 
gingen wij kijken of dingen met elkaar te maken hadden. En eigenlijk merkten 
we dat wij alles met elkaar konden verbinden. Allemaal elementen die we tot 
3 verschillende concept konden verbinden.

Ik kwam op het idee dat het hele groepje over die 3 concepten gingen 
uitschrijven tot een goed idee. Deze ideeën lazen we aan elkaar voor en 
gingen we kijken welk idee het beste was. Van deze 3 ideeën gingen we met
zijn alle voor en nadelen opschrijven. Weer was niks verkeerd en alles goed. 
Het was iedereen zijn eigen mening.



![alt tekst](../img/COCD-BOX-1.jpg)



![alt tekst](../img/COCD-BOX-2.jpg)