---
title: Presentatie
subtitle: woensdag 4 oktober
date: 2017-10-04
---

Vandaag de dag van de presentatie!
Van te voren heb ik de presentatie in elkaar gezet en iedereen was 
wat eerder op school gekomen om deze nog even door te nemen.
Ik had er een goed gevoel over we waren goed voorbereid op de presentatie! 
Het was wel spannend omdat er ook een alumni en een vakdocent bij was. 
En het belangrijk was dat het duidelijk over kwam. 

We waren aan de beurt en hadden 4 minuten de tijd. We begonnen met ons 
onderzoek die we visueel hadden gemaakt. Daarna kwam het prototype en ik 
sta het spel uit te leggen en ik merk aanmezelf dat ik er blij van word en 
daarom teveel in detail treed. Ja.. daar ging het belletje de 4 minuten 
waren om! en we waren nog niet klaar. Ik baalde hiervan omdat we de belangrijke 
elementen niet konden vertellen.

Toen kwam de feedback:
Over het algemeen was ons concept goed bedacht en vonden zij het ook een leuk 
spel. Het werd duidelijk uitgelegd met een intro.
Opdrachten zijn passend bij de doelgroep.
Geslaagd spel, geven zelf al verbeter punten aan.

Er waren ook nog wat aanmerkingen over het concept:
De totale uitleg van het spel was een beetje onduidelijk.
Bedenk wel of geen deuren?

Aansluitend met de doelgroep, ja.
Het thema en de opdrachten passen heel goed bij de doelgroep.
Vooral dat je pas bij het eerste punt groepjes maakt werd gewaardeerd.
Wat is de connectie tussen de deuren en de studenten, kijk daar nog even 
goed naar wat jullie hiermee willen bereiken in het spel.

Eisen opdrachtgever, ja
De eisen van de opdrachtgever zijn in het spel aanwezig.

Presentatie, ja
Goede presentatie in het geheel.

Mooi paper prototype.
Goed dat jullie al feedback gaven op jullie spel achteraf.

Met deze feedback kunnen we na het weekend verder om te verwerken
voor de laatste iteratie.
